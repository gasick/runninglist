// struct.go
package task

import "time"

func taskExample() (tasks []Task) {

	week := time.Now().YearDay() / 7
	tasks = append(tasks, returnTestTasks(week)...)
	//tasks = append(tasks, returnTestTasks(week+1)...)
	//tasks = append(tasks, returnTestTasks(week-1)...)
	return
}

func returnTestTasks(week int) []Task {
	return []Task{
		Task{
			Description: "создано в пн, выполнено в ср",
			Done:        time.Wednesday,
			CreateDay:   time.Monday,
			Week:        week,
		},
		Task{
			Description: "создано в ср, не выполнено",
			Done:        -1,
			CreateDay:   time.Wednesday,
			Week:        week,
		},
		Task{
			Description: "Создано и выполнено во вт.",
			Done:        time.Tuesday,
			CreateDay:   time.Tuesday,
			Week:        week,
		},
		Task{
			Description: "создано в вс, выполнено в пн",
			Done:        time.Monday,
			CreateDay:   time.Sunday,
			Week:        week,
		},
	}
}
