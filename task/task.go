// struct.go
package task

import (
	"runninglist/utils"
	"time"
)

type Task struct {
	Description string
	Done        time.Weekday
	CreateDay   time.Weekday
	Year        int
	Week        int
}

func NewTask(dayOfWeek time.Weekday, weekNumber int, description string) (t Task) {
	t.Description = description
	t.Done = -1
	t.CreateDay = dayOfWeek
	t.Week = weekNumber
	t.Year = time.Now().Year()
	return
}

func (t *Task) MakeDone() {
	if t.Done == -1 {
		t.Done = utils.Day.Weekday()
		return
	}
	t.Done = -1
}
