// main.go
package main

import (
	"runninglist/menustate"
	"runninglist/utils"
	"time"
)

func main() {
	utils.Day = time.Now()
	g := menustate.NewMenuState(0)
	defer g.Tasks.Write()
	g.PrintTasks()
	menustate.Input(g)
}
