module runninglist

go 1.21.1

require (
	github.com/eiannone/keyboard v0.0.0-20220611211555-0d226195f203 // indirect
	github.com/mattn/go-sqlite3 v1.14.17 // indirect
	github.com/pelletier/go-toml/v2 v2.0.2 // indirect
	github.com/valerianomacuri/lowdb v0.6.0 // indirect
	golang.org/x/sys v0.0.0-20220520151302-bc2c85ada10a // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
