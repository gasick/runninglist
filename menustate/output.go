// show.go
package menustate

import (
	"fmt"
	"runninglist/task"
	"runninglist/utils"
)

func (g *MenuState) PrintTasks() {
	g.Tasks.Read()
	lineNumber := 1
	_, week := utils.Day.ISOWeek()
	fmt.Print("\033[3;J\033[H\033[2J")
	g.printCanvas()
	weekTasks := g.FilterTaskForWeekAndYear()
	for i, t := range weekTasks {
		printWeekTasks(
			lineNumber,
			t,
			(i == g.ActiveLine),
			(g.ActiveWeek == week),
		)
		lineNumber++
	}
}

func printWeekTasks(number int, t task.Task, isActiveLine, printCurrentWeekTasks bool) {
	weekDays := [7]int{0, 0, 0, 0, 0, 0, 0}
	switch t.Done {
	case -1:
		if printCurrentWeekTasks {
			for i := t.CreateDay; i <= utils.Day.Weekday(); i++ {
				weekDays[i] = 2
			}
		} else {
			for i := t.CreateDay; i <= utils.Day.Weekday(); i++ {
				weekDays[i] = 0
			}
		}
	default:
		for i := t.CreateDay; i < t.Done; i++ {
			weekDays[i] = 2
		}
		weekDays[t.Done] = 1
	}

	fmt.Printf(
		"\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%d) %s\n",
		printDays(weekDays[0]),
		printDays(weekDays[1]),
		printDays(weekDays[2]),
		printDays(weekDays[3]),
		printDays(weekDays[4]),
		printDays(weekDays[5]),
		printDays(weekDays[6]),
		markActiveLine(isActiveLine),
		number,
		t.Description,
	)
}

func (g *MenuState) printCanvas() {
	_, currentWeek := utils.Day.ISOWeek()
	if g.ActiveWeek == currentWeek {
		fmt.Printf("\tCurrent week %d, year %d\t\t%s\n", (g.ActiveWeek), g.ActiveYear, utils.Day.Format("\t\t| 15:04\t01-02-2006"))
	} else {
		fmt.Printf("\tWeek %d, Year %d\t\t%s\n", (g.ActiveWeek), g.ActiveYear, utils.Day.Format("\t\t| 15:04\t01-02-2006"))
	}
	fmt.Printf("\tsun\tmon\ttue\twen\tthu\tfri\tsat\t|\tTasks\n")
	fmt.Printf("\t########################################################|#####################################\n")
}

func markActiveLine(active bool) string {
	if !active {
		return "|"
	}
	return "| *"
}

func printDays(i int) string {
	switch i {
	case 1:
		return "[X]"
	case 2:
		return "[ ]"
	default:
		return "   "
	}
}
