package menustate

import (
	"bufio"
	"fmt"
	"github.com/eiannone/keyboard"
	"github.com/valerianomacuri/lowdb/adapters"
	"github.com/valerianomacuri/lowdb/low"
	"os"
	"runninglist/task"
	"runninglist/utils"
	"strings"
	"time"
)

type MenuState struct {
	ActiveLine                          int
	ActiveWeek                          int
	ActiveYear                          int
	ShowAllUndoneTasksBeforeCurrentDate bool
	Tasks                               *low.Low[[]task.Task]
}

func NewMenuState(line int) (g *MenuState) {
	g = &MenuState{}
	_, week := utils.Day.ISOWeek()
	g.ActiveWeek = week
	g.ActiveLine = line
	g.ActiveYear = time.Now().Year()
	g.ShowAllUndoneTasksBeforeCurrentDate = false
	adapter := adapters.NewJSONFile[[]task.Task]("db.json")
	g.Tasks = low.New[[]task.Task](adapter)
	g.Tasks.Read()
	return
}
func Input(g *MenuState) {
	utils.Day = time.Now()
	if err := keyboard.Open(); err != nil {
		panic(err)
	}
	defer func() {
		_ = keyboard.Close()
	}()

	//fmt.Println("Press q to quit")
	for {
		char, _, err := keyboard.GetKey()
		if err != nil {
			panic(err)
		}
		g.ShowMainMenu(char)
		//fmt.Printf("You pressed: rune %q, key %X\r\n", char, "")
		if char == 'q' {
			return
		}
	}
}

func (g *MenuState) FilterTaskForWeekAndYear() (output []task.Task) {
	if g.ShowAllUndoneTasksBeforeCurrentDate {
		for _, t := range g.Tasks.Data {
			if (t.Year == g.ActiveYear && t.Week == g.ActiveWeek) ||
				(t.Year == g.ActiveYear && t.Week < g.ActiveWeek && t.Done == -1) {
				output = append(output, t)
			}
		}
		return output
	}
	for _, t := range g.Tasks.Data {
		if t.Week == g.ActiveWeek && t.Year == g.ActiveYear {
			output = append(output, t)
		}
	}
	return output
}

func (g *MenuState) inputNewTask() {
	_, week := utils.Day.ISOWeek()
	if g.ActiveWeek < week {
		fmt.Println("\t\n\t Use current or next week to create task")
		time.Sleep(5 * time.Second)
		return
	}

	reader := bufio.NewReader(os.Stdin)
	fmt.Println("Enter new task")
	fmt.Println("---------------------")

	fmt.Print("-> ")
	text, _ := reader.ReadString('\n')
	text = strings.TrimRight(text, "\r\n")
	// convert CRLF to LF
	//log.Println("Create week day: ", utils.Day.Weekday())
	if g.ActiveWeek == week {
		t := task.NewTask(utils.Day.Weekday(), g.ActiveWeek, text)
		g.Tasks.Data = append(g.Tasks.Data, t)
	} else {
		t := task.NewTask(time.Sunday, g.ActiveWeek, text)
		g.Tasks.Data = append(g.Tasks.Data, t)
	}
	g.Tasks.Write()
}
