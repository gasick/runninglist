package menustate

import (
	"log"
)

func (g *MenuState) ShowMainMenu(input rune) {
	switch input {
	case 'h':
		g.switchToPreviousWeek()
		break
	case 'k':
		g.moveActiveLineUp()
		break
	case 'j':
		g.moveActiveLineDown()
		break
	case 'u':
		g.showAndNotDoneBeforeTask()
		break
	case 'l':
		g.switchToNextWeek()
		break
	case 'n':
		g.inputNewTask()
		break
	case '?':
		log.Println("Help is here")
		break
	case '\x00':
		list := g.FilterTaskForWeekAndYear()
		for i := range g.Tasks.Data {
			if (list)[g.ActiveLine].Description == g.Tasks.Data[i].Description {
				g.Tasks.Data[i].MakeDone()
			}
		}
		g.Tasks.Write()
		break
	default:
		break
	}
	g.PrintTasks()

}

func (g *MenuState) showAndNotDoneBeforeTask() {
	if g.ShowAllUndoneTasksBeforeCurrentDate {
		g.ShowAllUndoneTasksBeforeCurrentDate = false
		return
	}
	g.ShowAllUndoneTasksBeforeCurrentDate = true
}

func (g *MenuState) moveActiveLineDown() {
	if g.ActiveLine < len(g.FilterTaskForWeekAndYear())-1 {
		g.ActiveLine = g.ActiveLine + 1
	}
}

func (g *MenuState) moveActiveLineUp() {
	if g.ActiveLine > 0 {
		g.ActiveLine = g.ActiveLine - 1
	}
}

func (g *MenuState) switchToPreviousWeek() {
	if (g.ActiveWeek - 1) < 1 {
		g.ActiveWeek = 52
		g.ActiveLine = 0
		g.ActiveYear = g.ActiveYear - 1
		return
	}
	g.ActiveWeek = g.ActiveWeek - 1
	g.ActiveLine = 0
}
func (g *MenuState) switchToNextWeek() {
	if (g.ActiveWeek + 1) > 52 {
		g.ActiveWeek = 1
		g.ActiveLine = 0
		g.ActiveYear = g.ActiveYear + 1
		return
	}
	g.ActiveWeek = g.ActiveWeek + 1
	g.ActiveLine = 0
}
